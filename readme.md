# Code craker kata
Code Cracker

Problem Description
Given an alphabet decryption key like the one below, create a program that can crack any message using the decryption key.
Alphabet
```
a b c d e f g h i j k l m n o p q r s t u v w x y z

Decryption Key
! ) " ( £ * % & > < @ a b c d e f g h i j k l m n o
``` 
You could also create a encryption program that will encrypt any message you give it using the key.
https://codingdojo.org/kata/CodeCracker/


Reglas:
- Si es null = null
- Si es vacio = vacio
- Caracter que no forma parte de los caracters: lanzamos una excepción
  - valor vacio, ., retorno carro o caracter que pertence al Decryption key.


Bitacora:

Nos quedamos con el codigo roto. Estamos tratando de crear
objetos con lo que hemos:
- Creado el objeto secuencia y estamos refactorizando
  - _Ojo. Recomendado usar el parameterMethod. Esto es nuevo para Jose.
  - Devuelve directamente objetos y no primitivas.
- El siguiente paso es hacer mas legible el metodo CrackerShould
- Cuando tengamos esto pensar en si podemos aplicar algún patrón

```` Java
@Test
@Parameters(method = "parametersToTestAdd")
public void whenWithNamedMethod_thenSafeAdd(
  int a, int b, int expectedValue) {
 
    assertEquals(expectedValue, serviceUnderTest.safeAdd(a, b));
}

private Object[] parametersToTestAdd() {
    return new Object[] { 
        new Object[] { 1, 2, 3 }, 
        new Object[] { -10, 30, 20 }, 
        new Object[] { Integer.MAX_VALUE, 2, Integer.MAX_VALUE }, 
        new Object[] { Integer.MIN_VALUE, -8, Integer.MIN_VALUE } 
    };
}
```