package org.example;

import java.util.Iterator;

public class SequenceChar implements Iterator<Character> {
    private final int index;
    private String value;

    public SequenceChar(String value) throws NotAllowed {
        if(value == null || value.isEmpty()) {
            throw new NotAllowed();
        }
        this.value=value;
        index =0;
    }

    @Override
    public boolean hasNext() {
        return (index < value.length());
    }

    @Override
    public Character next() {
        return value.charAt(index);
    }
}
