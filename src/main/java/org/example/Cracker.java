package org.example;

public class Cracker {

    private final Translator translator;

    public Cracker(Translator translator) {
        this.translator = translator;
    }

    public String of(SequenceChar sequence) throws NotAllowed {
        String result = "";
        for (SequenceChar it = sequence; it.hasNext(); ) {
            char singleValue = it.next();
            translator.checkValidValue(singleValue);
            result += translator.getTranslation(singleValue);
        }



        return result;
    }
}
