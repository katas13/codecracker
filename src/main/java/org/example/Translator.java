package org.example;

import java.util.HashMap;

public class Translator {
    HashMap<Character, Character> map = new HashMap<Character, Character>() {
        {
            put('a', '!');
            put('b', ')');
            put('c', '"');
            put('d', '(');
            put('e', '£');
            put('f', '*');
            put('g', '%');
            put('h', '&');
            put('i', '>');
            put('j', '<');
            put('k', '@');
            put('l', 'a');
            put('m', 'b');
            put('n', 'c');
            put('o', 'd');
            put('p', 'e');
            put('q', 'f');
            put('r', 'g');
            put('s', 'h');
            put('t', 'i');
            put('u', 'j');
            put('v', 'k');
            put('w', 'l');
            put('x', 'm');
            put('y', 'n');
            put('z', 'o');
        }
    };

    protected void checkValidValue(char value) throws NotAllowed {
        if (!map.containsKey(value)) {
            throw new NotAllowed();
        }
    }

    protected Character getTranslation(char singleValue) {
        return map.get(singleValue);
    }
}
