package org.example;



import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;


public class SequenceShould {

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {""})
    public void return_exception_when_character_is_not_valid(String value) {
        Assertions.assertThrows(NotAllowed.class, () -> {
            new SequenceChar(value);
        });
    }


}
