package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CrackerShould {

    @ParameterizedTest
    @CsvSource({
            "a,!",
            "b,)",
            "c,\"",
            "d,(",
            "e,£",
            "f,*",
            "g,%",
            "h,&",
            "i,>",
            "j,<",
            "k,@",
            "l,a",
            "m,b",
            "n,c",
            "o,d",
            "p,e",
            "q,f",
            "r,g",
            "s,h",
            "t,i",
            "u,j",
            "v,k",
            "w,l",
            "x,m",
            "y,n",
            "z,o"
    })
    public void convert_a_valid_character(String value, String expected) throws NotAllowed {

        assertEquals(expected, getCharacterCrack(value));
    }


    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {"!", "£",""})
    public void return_exception_when_character_dont_belong_alphabet(String value) {
        Assertions.assertThrows(NotAllowed.class, () -> {
            getCharacterCrack(value);
        });
    }

    @ParameterizedTest
    @CsvSource({
            "ab,!)",
            "acdzy,!\"(on",
            "abcdefghijklmnopqrstuvwxyz,!)\"(£*%&><@abcdefghijklmno"
    })
    public void convert_several_valid_characters(String value, String expected) throws NotAllowed {
        assertEquals(expected, getCharacterCrack(value));
    }

    private String getCharacterCrack(SequenceChar a) throws NotAllowed {

        Cracker cracker = new Cracker(new Translator());
        String crack = cracker.of(a);
        return crack;
    }


}
